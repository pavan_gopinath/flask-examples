from flask import Flask, jsonify
import time
from datetime import datetime
app = Flask(__name__)

@app.route("/")
def welcome():
    return "Flask CI CD"
    
@app.route("/v1/time")
def get_time():
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    return jsonify({"Time": dt_string})

@app.route('/v1/test/<name>')
def get_name(name):
    return jsonify({"name":name})

@app.route('/v1/get_palindrom/<s>')
def get_longest_palindrome(s):
    p_list = []
    for i in range(0, len(s)+1):
        l = s[i:]
        for j in range(0,len(l)+1):
            if l[:j] == l[:j][::-1]:
                p_list.append(l[:j])
                p_list = sorted(p_list, reverse=True)
                res = {_:len(_) for _ in p_list if len(_) > 1}
    return jsonify(res)
                
        
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')

